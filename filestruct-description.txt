Records of your data files contain the following fields in the following sequence.
 
 
The field 'insurance' shall be an integer occupying thirty two bits  
The field 'servant' shall be a floating point number occupying sixty four bits  
The field 'mint' shall be an integer occupying eight bits  
The field 'range' shall be an unsigned integer occupying sixteen bits  
The field 'peace' shall be a Boolean occupying eight bits  
The field 'slave' shall be an integer occupying sixty four bits  
The field 'scissors' shall be a character occupying eight bits  
The field 'breath' shall be a fixed-length string holding at most 7 characters (including terminating nulls)  
The field 'songs' shall be an unsigned integer occupying sixteen bits  
The field 'eye' shall be a Boolean occupying sixteen bits  
The field 'toe' shall be a Boolean occupying sixteen bits  
The field 'rate' shall be a Boolean occupying sixteen bits  
The field 'juice' shall be a floating point number occupying sixty four bits  
The field 'harmony' shall be a Boolean occupying eight bits  
The field 'daughter' shall be a character occupying eight bits  
The field 'texture' shall be a floating point number occupying thirty two bits  
